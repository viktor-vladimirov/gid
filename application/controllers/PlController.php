<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\models\Pladmin;
use application\models\Pl;

class PlController extends Controller {

    public function indexAction() {
        $pagination = new Pagination($this->route, $this->model->PlPostsCount(), 3);
        $vars = [
            'pagination' => $pagination->get(),
            'list' => $this->model->PlPostsList($this->route),
        ];
        $this->view->render('Get It Done', $vars);
    }

    public function aboutusAction() {
        $this->view->render('About Us');
    }

    public function projectsAction() {
        $pagination = new Pagination($this->route, $this->model->PlPostsCount(), 3);
        $vars = [
            'pagination' => $pagination->get(),
            'list' => $this->model->PlPostsList($this->route),
        ];
        $this->view->render('Projekty', $vars);
    }

    public function eventAction() {
        $this->view->render('event');
    }

    public function contactAction() {
        if (!empty($_POST)) {
            if (!$this->model->PlcontactValidate($_POST)) {
                $this->view->message('Error', $this->model->error);
            }

            define('SECRET_KEY', '6LedksMUAAAAAAAq1PkKzVo7U6c61nmnpDfB_qxM');

            function getCaptcha($secretKey) {
                $Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . SECRET_KEY . "&response={$secretKey}");
                $return = json_decode($Response);
                return $return;
            }

            $return = getCaptcha($_POST['g-recaptcha-response']);

            if ($return->success == true && $return->score > 0.5) {

                $to = 'info@gid.company';
                $sub = 'сообщение из формы сайта (Pl)';
                $from = $_POST['email'];

                $subject = '=?UTF-8?B?' . base64_encode($sub) . '?=';

                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/plain; charset=utf-8\r\n";
                $headers .= "From: $from\r\n";

                $message = 'Отправитель: ' . $_POST['name'] . "\r\n" . $_POST['text'];

                mail($to, $subject, $message, $headers);

                $this->view->message('Wiadomość została wysłana', 'Dzieńkujemy!');
            } else {
                $this->view->message('Wiadomość nie została wysłana', 'Niepoprawna CAPTCHA');
            }
        }

        $this->view->render('Kontakt');
    }

    public function postAction() {
        $adminModel = new Pladmin;
        if (!$adminModel->isPostExists($this->route['id'])) {
            $this->view->errorCode(404);
        }
        $vars = [
            'data' => $adminModel->postData($this->route['id'])[0],
        ];
        $this->view->render('Projekty', $vars);
    }

    public function postsAction() {

        $mainModel = new Pl;
        $list = $mainModel->projectList();

        $vars = [
            'list' => $list,
        ];

        $this->view->render('Posts', $vars);
    }

}
