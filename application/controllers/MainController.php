<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\Pagination;
use application\models\Admin;
use application\models\Main;

class MainController extends Controller {


	public function indexAction() {
		$pagination = new Pagination($this->route, $this->model->postsCount(), 3);
		$vars = [
			'pagination' => $pagination->get(),
			'list' => $this->model->postsList($this->route),
		];
		$this->view->render('Get It Done', $vars);
		
	}
	
	public function aboutusAction () {		
		$this->view->render('About Us');		
	}

	public function projectsAction () {
		$pagination = new Pagination($this->route, $this->model->postsCount(), 3);
		$vars = [
			'pagination' => $pagination->get(),
			'list' => $this->model->postsList($this->route),
		];
		$this->view->render('Projects', $vars);		
	}



	public function eventAction () {		
		$this->view->render('event');		
	}

	public function contactAction() {
		if (!empty($_POST)) {
			if (!$this->model->contactValidate($_POST)) {
				$this->view->message('Внимание', $this->model->error);
			}
			
			define('SECRET_KEY', '6LedksMUAAAAAAAq1PkKzVo7U6c61nmnpDfB_qxM');
			
			function getCaptcha($secretKey){
				$Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response={$secretKey}");
				$return = json_decode($Response);
				return $return;
				}

				$return = getCaptcha($_POST['g-recaptcha-response']);

			if ($return->success == true && $return->score > 0.5) {


					$to = 'info@gid.company';
					$sub =  'сообщение из формы сайта';
					$from  = $_POST['email'];

					$subject = '=?UTF-8?B?' . base64_encode($sub) . '?=';
				 
				    $headers  = "MIME-Version: 1.0\r\n"; 
				    $headers .= "Content-type: text/plain; charset=utf-8\r\n";
				    $headers .= "From: $from\r\n";
		 
				    $message =  'Отправитель: '.$_POST['name']. "\r\n" .$_POST['text'];

					mail($to, $subject, $message, $headers);	
 
 
				$this->view->message('Сообщение отправлено', 'Спасибо!');
			} else {
				$this->view->message('Сообщение не отправлено', 'Капча не пройдена');	
			}			
		}
		
		$this->view->render('Contacts');
	}

	public function postAction () {	
		$adminModel = new Admin;	
		if (!$adminModel->isPostExists($this->route['id'])) {
			$this->view->errorCode(404);
		}
		$vars = [
			'data' => $adminModel->postData($this->route['id'])[0],
		];
		$this->view->render('Projects', $vars);		
	}
        
        public function postsAction() {

        $mainModel = new Main;
        $list = $mainModel->projectList();

        $vars = [
            'list' => $list,
        ];

        $this->view->render('Posts', $vars);
    }

}