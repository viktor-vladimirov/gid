<?php

return [

	/* GUI routes */

	// MainController
	'' => [
		'controller' => 'main',
		'action' => 'index',
	],

	'main/index/{page:\d+}' => [
		'controller' => 'main',
		'action' => 'index',
	],
	'aboutus' => [
		'controller' => 'main',
		'action' => 'aboutus',
	],
	
	'ru' => [
		'controller' => 'main',
		'action' => 'ru',
	],

	'contact' => [
		'controller' => 'main',
		'action' => 'contact',
	],

	'event' => [
		'controller' => 'main',
		'action' => 'event',
		],
	
	'post/{id:\d+}' => [
		'controller' => 'main',
		'action' => 'post',
	],
    
	'posts' => [
		'controller' => 'main',
		'action' => 'posts',
	],    

	'projects' => [
		'controller' => 'main',
		'action' => 'projects',
	],

	'main/projects/{page:\d+}' => [
		'controller' => 'main',
		'action' => 'projects',
	],



	//en controller

	'en' => [
		'controller' => 'en',
		'action' => 'index',
	],
	'en/index/{page:\d+}' => [
		'controller' => 'en',
		'action' => 'index',
	],
	'en/aboutus' => [
		'controller' => 'en',
		'action' => 'aboutus',
	],

	'en/contact' => [
		'controller' => 'en',
		'action' => 'contact',
	],

	'en/event' => [
		'controller' => 'en',
		'action' => 'event',
		],
	
	'en/post/{id:\d+}' => [
		'controller' => 'en',
		'action' => 'post',
	],
    
	'en/posts' => [
		'controller' => 'en',
		'action' => 'posts',
	],     

	'en/projects' => [
		'controller' => 'en',
		'action' => 'projects',
	],

	'en/projects/{page:\d+}' => [
		'controller' => 'en',
		'action' => 'projects',
	],

	//pl controller

	'pl' => [
		'controller' => 'pl',
		'action' => 'index',
	],
	'pl/index/{page:\d+}' => [
		'controller' => 'pl',
		'action' => 'index',
	],
	'pl/aboutus' => [
		'controller' => 'pl',
		'action' => 'aboutus',
	],

	'pl/contact' => [
		'controller' => 'pl',
		'action' => 'contact',
	],

	'pl/event' => [
		'controller' => 'pl',
		'action' => 'event',
		],
	
	'pl/post/{id:\d+}' => [
		'controller' => 'pl',
		'action' => 'post',
	],
    
	'pl/posts' => [
		'controller' => 'pl',
		'action' => 'posts',
	],

	'pl/projects' => [
		'controller' => 'pl',
		'action' => 'projects',
	],

	'pl/projects/{page:\d+}' => [
		'controller' => 'pl',
		'action' => 'projects',
	],

	/* ADMIN */


	// AdminController

	'admin/login' => [
		'controller' => 'admin',
		'action' => 'login',
	],
	'admin/logout' => [
		'controller' => 'admin',
		'action' => 'logout',
	],
	'admin/add' => [
		'controller' => 'admin',
		'action' => 'add',
	],
	'admin/edit/{id:\d+}' => [
		'controller' => 'admin',
		'action' => 'edit',
	],
	'admin/delete/{id:\d+}' => [
		'controller' => 'admin',
		'action' => 'delete',
	],
	'admin/posts/{page:\d+}' => [
		'controller' => 'admin',
		'action' => 'posts',
	],
	'admin/posts' => [
		'controller' => 'admin',
		'action' => 'posts',
	],

	// EnadminController

	'enadmin/add' => [
		'controller' => 'enadmin',
		'action' => 'add',
	],
	'enadmin/edit/{id:\d+}' => [
		'controller' => 'enadmin',
		'action' => 'edit',
	],
	'enadmin/delete/{id:\d+}' => [
		'controller' => 'enadmin',
		'action' => 'delete',
	],
	'enadmin/posts/{page:\d+}' => [
		'controller' => 'enadmin',
		'action' => 'posts',
	],
	'enadmin/posts' => [
		'controller' => 'enadmin',
		'action' => 'posts',
	],

	// PlnadminController

	'pladmin/add' => [
		'controller' => 'pladmin',
		'action' => 'add',
	],
	'pladmin/edit/{id:\d+}' => [
		'controller' => 'pladmin',
		'action' => 'edit',
	],
	'pladmin/delete/{id:\d+}' => [
		'controller' => 'pladmin',
		'action' => 'delete',
	],
	'pladmin/posts/{page:\d+}' => [
		'controller' => 'pladmin',
		'action' => 'posts',
	],
	'pladmin/posts' => [
		'controller' => 'pladmin',
		'action' => 'posts',
	],

];