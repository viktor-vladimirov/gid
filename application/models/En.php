<?php

namespace application\models;

use application\core\Model;

class En extends Model {

    public $error;

    public function EncontactValidate($post) {
        $nameLen = iconv_strlen($post['name']);
        $textLen = iconv_strlen($post['text']);
        if ($nameLen < 3 or $nameLen > 20) {
            $this->error = 'The name must be between 3 and 20 characters';
            return false;
        } elseif (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error = 'Invalid Email address';
            return false;
        } elseif ($textLen < 10 or $textLen > 500) {
            $this->error = 'The message must contain between 10 and 500 characters';
            return false;
        }
        return true;
    }

    public function EnPostsCount() {
        return $this->db->column('SELECT COUNT(id) FROM posts1');
    }

    public function EnPostsList($route) {
        $max = 3;
        $params = [
            'max' => $max,
            'start' => ((($route['page'] ?? 1) - 1) * $max),
        ];
        return $this->db->row('SELECT * FROM posts1 ORDER BY id DESC LIMIT :start, :max', $params);
    }

    public function projectList() {

        $sql = 'SELECT * FROM posts1';

        return $this->db->row($sql);
    }

    public function mail_utf8($to, $from, $subject, $message) {

        $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/plain; charset=utf-8\r\n";
        $headers .= "From: $from\r\n";

        return mail($to, $subject, $message, $headers);
    }

}
