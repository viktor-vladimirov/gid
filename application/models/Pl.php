<?php

namespace application\models;

use application\core\Model;

class Pl extends Model {

    public $error;

    public function PlcontactValidate($post) {
        $nameLen = iconv_strlen($post['name']);
        $textLen = iconv_strlen($post['text']);
        if ($nameLen < 3 or $nameLen > 20) {
            $this->error = 'Imię musi zawierać od 3 do 20 znaków';
            return false;
        } elseif (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error = 'Niepoprawny adres e-mail';
            return false;
        } elseif ($textLen < 10 or $textLen > 500) {
            $this->error = 'Wiadomość musi zawierać od 10 do 500 znaków';
            return false;
        }
        return true;
    }

    public function PlPostsCount() {
        return $this->db->column('SELECT COUNT(id) FROM posts2');
    }

    public function PlPostsList($route) {
        $max = 3;
        $params = [
            'max' => $max,
            'start' => ((($route['page'] ?? 1) - 1) * $max),
        ];
        return $this->db->row('SELECT * FROM posts2 ORDER BY id DESC LIMIT :start, :max', $params);
    }

    public function projectList() {

        $sql = 'SELECT * FROM posts2';

        return $this->db->row($sql);
    }

    public function mail_utf8($to, $from, $subject, $message) {

        $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/plain; charset=utf-8\r\n";
        $headers .= "From: $from\r\n";

        return mail($to, $subject, $message, $headers);
    }

}
