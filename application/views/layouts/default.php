<!DOCTYPE html>
<html lang="en">
<html lang="pl">
    <head>
        <title><?php echo $title; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1 minimum-scale=1 maximum-scale=1" >  
        <link href="/public/styles/style.css" rel="stylesheet">
        <link href="/public/styles/dropdown.css" rel="stylesheet">



        <link href="https://fonts.googleapis.com/css?family=Exo+2:400,500,500i&display=swap&subset=cyrillic" rel="stylesheet"> 
        <link rel="shortcut icon" href="/public/images/logom.png">
        
        <?php define('SITE_KEY', '6LedksMUAAAAAJlYerCiRIe7dSzSC94_yoGWmCOW'); ?>
        <link href="/public/styles/postblock.css" rel="stylesheet">
        <link href="/public/styles/post.css" rel="stylesheet">        
        <link href="/public/styles/pagination.css" rel="stylesheet">  
        <script src="/public/scripts/jquery.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js?render=<?php echo SITE_KEY; ?>"></script>
        <script>
        grecaptcha.ready(function() {
        grecaptcha.execute('<?php echo SITE_KEY; ?>', {action: 'homepage'}).then(function(token) {
        console.log(token);
        document.getElementById('g-recaptcha-response').value=token;
        });
        });
        </script>
        <script src="/public/scripts/popper.js"></script>
        <script src="/public/scripts/anchor.js"></script>
        <script src="/public/scripts/form.js"></script>
        <script src="/public/scripts/bootstrap.js"></script>        
    </head>
        <?php echo $content; ?>

</html>
