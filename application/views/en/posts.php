
    <div class="block-A">

        <div class="menu">
        <a class="menu-left" href="/en"></a>

        <div class="dropdown">
              <button class="mainmenubtn"></button>
              <div class="dropdown-child">
                <div class="drop">
                <a href="/en" class="close"></a>
                </div>
                <div class="dropr1">                
                <a href="/" class="launge1">ru</a>
                <a href="/pl" class="launge2">pl</a>
                <a href="/en" class="launge3">en</a>
                </div>
                <div class="dropr2">
                <a href="/en/projects">Projects</a>
                <a href="/en/aboutus">About us</a>
                <a href="/en/contact">Contacts</a>
                </div>
              </div>
        </div>

        <div class="menu-right">
            <a href="/pl">pl</a>
            <a href="/en">en</a>
            <a href="/">ru</a>
        </div>
        </div>
        <div class="menu-right1">
            <a href="/en/posts">Projects</a>
            <a href="/en/aboutus">About us</a>
            <a href="/en/#contacts">Contacts</a>
        </div>
        <div class="menu1">
            <p>Projects</p>
            <hr>
            <h1>GID is the development of effective projects aimed to solve our customer's marketing issues to provide with the successful promotion.</h1>
        </div>
        <div class="menu2">
            <a href="/en/#strategy">Strategy</a>
            <a href="/en/#analytics">Analytics</a>
            <a href="/en/#branding">Branding</a>
            <a href="/en/#klienting">Clienting</a>
        </div>
    </div>
    

<!-- # -->
<div class="Posts-list">
            <?php if (empty($vars['list'])): ?>
                    <p>empty list</p>
            <?php else: ?>
                <?php foreach ($vars['list'] as $val): ?>

                    <div class="Posts-wrapper">
                        <a href="/post/<?php echo $val['id']; ?>">
                            <div class="Posts-container" style="background-image: url('/public/materials/300x167/<?php echo $val['id']; ?>.jpg')">
                            </div>
                            <div class="subtitlecontainer">
                                <h2 class="post-subtitle"><?php echo htmlspecialchars($val['description'], ENT_QUOTES); ?></h2>
                            </div>
                            <h5 class="post-title"><?php echo htmlspecialchars($val['name'], ENT_QUOTES); ?></h5> 
                        </a>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>     
</div>
<!-- # -->
 


        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>
