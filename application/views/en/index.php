<div class="wrapper">

    <div class="block-A">

        <div class="menu">
        <a class="menu-left" href="/en"></a>

        <div class="dropdown">
              <button class="mainmenubtn"></button>
              <div class="dropdown-child">
                <div class="drop">
                <a href="/en" class="close"></a>
                </div>
                <div class="dropr1">                
                <a href="/" class="launge1">ru</a>
                <a href="/pl" class="launge2">pl</a>
                <a href="/en" class="launge3">en</a>
                </div>
                <div class="dropr2">
                <a href="/en/projects">Projects</a>
                <a href="/en/aboutus">About us</a>
                <a href="/en/contact">Contacts</a>
                </div>
              </div>
        </div>

        <div class="menu-right">
            <a href="/pl">pl</a>
            <a href="/en">en</a>
            <a href="/">ru</a>
        </div>
        </div>
        <div class="menu-right1">
            <a href="/en/posts">Projects</a>
            <a href="/en/aboutus">About us</a>
            <a href="#contacts">Contacts</a>
        </div>
        <div class="menu1">
            <p>A full range of marketing services</p>
            <hr>
            <h1>GID is the development of effective projects aimed to solve our customer's marketing issues to provide with the successful promotion.</h1>
        </div>
        <div class="menu2">
            <a href="#strategy">Strategy</a>
            <a href="#analytics">Analytics</a>
            <a href="#branding">Branding</a>
            <a href="#klienting">Clienting</a>
        </div>


    </div>

    <!-- ###### BLOK B ####### -->

    <div class="block-B">
        
        <div class="row1"> 
          <table>
            <tr>
            <td class="tb1"><img src="/public/images/clienci.png" alt="img" width="75" height="75"></td>
            <td class="tittlet">We know where your clients are</td>
           
       
                        
            <td><img src="/public/images/decyzje.png" alt="img" width="65" height="75"></td>
            <td class="tittlet">We make decisions</td>
            
      
            <td><img src="/public/images/ceny.png" alt="img" width="70" height="75"></td>
            <td class="tittlet">We offer flexiable pricing</td>
            </tr>
          </table>
        </div>


        <div class="row1m"> 

          <table>                      
            <td><img src="/public/images/decyzje.png" alt="img" width="67" height="75"></td>
            <td class="tittlet">We make decisions</td>
          </table>  

          <table>
            <tr>
            <td><img src="/public/images/clienci.png" alt="img" width="75" height="75"></td>
            <td class="tittlet">We know where your clients are</td>
          </table>
       
      
          <table>   
            <td><img src="/public/images/ceny.png" alt="img" width="70" height="75"></td>
            <td class="tittlet">We offer flexiable pricing</td>
            </tr>
          </table>
        </div>


                   <!-- Блок дублируемый -2-3-4-5- -->

          <!-- ** СТРАТЕГИЯ ** -->

        <div class="row2">
            <div class="row2Left">
            <section id="strategy" ><h1>Strategy</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Have you thought of which way to develop your business</p>
                <p>Looking for new markets</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
            <section id="strategy" ><h1>Strategy</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1m">
                <p>Have you thought of which way to develop your business
                Looking for new markets</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>We can offer you an effective solution 
                based on YOUR task and OUR Analytics</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>We can offer you an effective solution </p>
                <p>based on YOUR task and OUR Analytics</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>Analysis of possible sales strategies</li>
                <li>Development of promotion  hypotheses and entry into new markets</li>
                <li>Offers on new products and promotion channels</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>Analysis of possible sales strategies</li>
                <li>Development of promotion  hypotheses and entry into new markets</li>
                <li>Offers on new products and promotion channels</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Get strategy!</a>
        </div>


        <!-- ** АНАЛИТИКА ** -->


        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
            <section id="analytics" ><h1>Analytics</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Are you at the process of planning to enter new markets</p>
                <p>and of developing promotion strategies</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
             <section id="analytics" ><h1>Analytics</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1m">
                <p>Are you at the process of planning to enter new markets and
                of developing promotion strategies</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>We will confirm your hypotheses with figures and calculations</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>We will confirm your hypotheses with figures and calculations</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>Determine the purpose of the research</li>
                <li>Choose methods and tools</li>
                <li>Collect and analyze information</li>
                <li>Provide a report in a convenient form with conclusions and recommendations</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>Determine the purpose of the research</li>
                <li>Choose methods and tools</li>
                <li>Collect and analyze information</li>
                <li>Provide a report in a convenient form with conclusions and recommendations</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Get conclusions!</a>
        </div>

      <!-- ** АНАЛИТИКА ** -->
        
       <!--# Блок дублируемый -2-3-4-5--->

        <!-- ** БРЕНДИНГ ** -->


        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
            <section id="branding" ><h1>Branding</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>You have a new good product</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
            <section id="branding" ><h1>Branding</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1mBr">
                <p>You have a new good product</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>We know how to make your product presentable</p>
                <p>and salable - naming is our cup of tea!</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>We know how to make your product presentable</p>
                <p>and salable - naming is our cup of tea!</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>We audit the brand and develop proposals (hypotheses) for its promotion</li>
                <li>Develop brand positioning strategy and brand platform</li>
                <li>Create brand communication strategies.</li>
                <li>We select the optimal channels of promotion</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>We audit the brand and develop proposals (hypotheses) for its promotion</li>
                <li>Develop brand positioning strategy and brand platform</li>
                <li>Create brand communication strategies.</li>
                <li>We select the optimal channels of promotion</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Get an idea!</a>
        </div>

      <!-- ** КЛИЕНТИНГ ** -->



        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
            <section id="klienting" ><h1>Clienting</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Looking for new points of interaction</p>
                <p>with your clients</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
            <section id="klienting" ><h1>Clienting</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1mBr">
                <p>Looking for new points of interaction with your clients</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>We are able to deliver  
                your offer to each client!</p> 
 
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>We are able to deliver</p>         
                <p>your offer to each client!</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>Analyze the client's insight, determine his needs and requests</li>
                <li>We offer the most effective "touch points" with the client</li>
                <li>Develop communication strategy</li>
                <li>Develop barriers to customers retention</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>Analyze the client's insight, determine his needs and requests</li>
                <li>We offer the most effective "touch points" with the client</li>
                <li>Develop communication strategy</li>
                <li>Develop barriers to customers retention</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Get more clients!</a>
        </div>

        <!-- ** Проекты ** -->

        <div class="row2">
            <div class="row2Left">
            <section id="projects" ><h1>Projects</h1></section>
            <hr>
            </div>
        </div>

        <div class="row2m">
            <div class="row2Leftm">
            <section id="projects" ><h1>Projects</h1></section>
            <hr>
            </div>
        </div>


         <!--Тус блок с постами -->

          <div class="container2">       
         
            <?php if (empty($vars['list'])): ?>
                <p>Список Новостей пуст</p>
            <?php else: ?>
                <?php foreach ($vars['list'] as $val): ?>
                    
                    <div class="wrapperpost">
                        <a href="/en/post/<?php echo $val['id']; ?>">
                        <div class="postcontainer" style="background-image: url('/public/materials/300x167/<?php echo $val['id']; ?>.jpg')">
                        </div>
                        <div class="subtitlecontainer">
                        <h2 class="post-subtitle"><?php echo htmlspecialchars($val['description'], ENT_QUOTES); ?></h2>
                        </div>
                        <h5 class="post-title"><?php echo htmlspecialchars($val['name'], ENT_QUOTES); ?></h5> 
                        </a>
                    </div>

                <?php endforeach; ?>
       
        <div class="row5">
            <a href="<?php echo '/en/posts';?>">See all projects</a>
        </div>
            <?php endif; ?>     
        </div>


         <!--Тус блок с постами -->


<div class="block-C ">

		<div class="Contacts-blockm">
			<div class="contacts-menu-m">
				<h1>Contacts</h1>
			</div>

            <section id="contacts" ></section>

            <div class="contacts-menu">
                <h1>Contacts</h1>
                <table>
                <tr>
                <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 723 40 25 </h2></td>     
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 535 594 761  </h2></td>  
                </tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 680 40 40</h2></td>  
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 515 952 543</h2></td>  
                </tr>                
                <tr>
                <tr>
                <td class="mail"><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                <td><h2>sales@gid.company</h2></td>  
                </tr>
                </table>
            </div>

			<div class="contacts-menu1-m">
				<div class="contactpost-m">
            		<form action="/en/contact" method="post">
                		<div class="contactpost1">                    
                        	<p><input type="text" class="form-control" name="name" placeholder="Name"></p>                  
                		</div>
                		<div class="contactpost2">             
                        	<p><input type="text" class="form-control" name="email" placeholder="E-mail"></p>
                		</div>
                		<div class="contactpost3">                    
                        	<p><textarea rows="5" class="form-control" name="text" placeholder="Message"></textarea></p>
                		</div>

                        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                		<br>
                		<div id="success">
                    		<button type="submit" class="btn btn-secondary" id="sendMessageButton">Contact us</button>     
                		</div>         
            		</form>
        		</div>
			</div>

                <div class="contacts-menu-m1">
                    <table>
                        <tr>
                            <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 723 40 25 </h2></td><td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 535 594 761  </h2></td>      
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 680 40 40</h2></td><td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 515 952 543</h2></td>   
                        </tr>
                        <tr>

                        </tr>   

                    </table>
                    <table align="center">
                        <tr >
                            <td class="mail" ><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                            <td><h2>sales@gid.company</h2></td>  
                        </tr>
                    </table> 
                </div>
				
		</div>

	</div>

        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>

</div>