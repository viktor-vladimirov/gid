    <div class="wrapper">


    <div class="block-A">

        <div class="menu">
        <a class="menu-left" href="/en"></a>

        <div class="dropdown">
              <button class="mainmenubtn"></button>
              <div class="dropdown-child">
                <div class="drop">
                <a href="/en" class="close"></a>
                </div>
                <div class="dropr1">                
                <a href="/" class="launge1">ru</a>
                <a href="/pl" class="launge2">pl</a>
                <a href="/en" class="launge3">en</a>
                </div>
                <div class="dropr2">
                <a href="/en/projects">Projects</a>
                <a href="/en/aboutus">About us</a>
                <a href="/en/contact">Contacts</a>
                </div>
              </div>
        </div>

        <div class="menu-right">
            <a href="/pl">pl</a>
            <a href="/en">en</a>
            <a href="/">ru</a>
        </div>
        </div>
        <div class="menu-right1">
            <a href="/en/posts">Projects</a>
            <a href="/en/aboutus">About us</a>
            <a href="#contacts">Contacts</a>
        </div>
        <div class="menu1">
            <p>About us</p>
            <hr>
            <h1>GID is the development of effective projects aimed to solve our customer's marketing issues to provide with the successful promotion.</h1>
        </div>
        <div class="menu2">
            <a href="/en/#strategy">Strategy</a>
            <a href="/en/#analytics">Analytics</a>
            <a href="/en/#branding">Branding</a>
            <a href="/en/#klienting">Clienting</a>
        </div>
    </div>

    <div class="block-D">

        <div class="row-us1">
            <h1>About us</h1>
            <hr>
        </div>

        <div class="row-us2">
            <text>
                If you have a necessity to research the market, analyze the activities of competitors, identify or create competitive advantages -
                our team - GID marketing consulting-  will help you to implement effective comprehensive marketing projects. If you plan to enter the CIS markets, we are ready to offer strategies
                of promotion, to create or adapt a brand, to develop product packaging design.
                Our team consists of practicing specialists with experience in marketing and sales at large commercial and industrial enterprises of Belarus.
                So,  we are ready to offer you...
            </text>
        </div>

        <div class="row-us1">
            <h1>GID Team</h1>
            <hr>
        </div>

        <div class="row-us3b1">
            <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Lubov Kovalyova</h2>
                <div class="Pb"><p>Strategy, Branding</p></div>
                <hr>
                <p>Work experience: 15 years</p>
                <p>Santa-Bremor Ltd.</p>
                <p>Santa-Rest Ltd.</p>
                <p> STiM Ltd.</p>
                <p>I may assist you in Russian</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 296 42 90 80</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>lubov.kovaleva@gid.company</h3></td>  
                </tr>
                </table>
            </div>

             <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Ekaterina Zarkova</h2>
                <div class="Pb"><p>Branding, Project Management</p></div>
                <hr>
                <p>Work experience: 8 years</p>
                <p>STiM Ltd.</p>
                <p>Transconsult Ltd.</p>
                <p>VRP Consulting Ltd.</p>
                <p>I may assist you in Russian, English</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 297 980 626</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                </tr>
                </table>
            </div>

             <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Dmitry Kovalyov</h2>
                <div class="Pb"><p>Project Management, Sales</p></div>
                <hr>
                <p>work experience 15 years at</p>
                <p>construction materials sales and transport services</p>
                <p>I may assist you in Russian, Polish</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+48 573 913 300</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>dmitry.kovalev@gid.company</h3></td>  
                </tr>
                </table>
            </div>
        </div>

        <div class="row-us3b2">
            <div class="persona1">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Irina Turanskaya</h2>
                <div><h4>Clienting, Analytics, Sales</h4></div>
                <hr>
                <p>Belfakta Media Ltd. team leader,</p>
                <p>STiM Ltd.marketing specialist</p>
                <p>Head of educational center "First step"</p>
                <p>I may assist you in Russian, English, Polish</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 29 723 40 25</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>irina.turanskaya@gid.company</h3></td>  
                </tr>
                </table>
            </div>    
         </div>   

        <!-- m -->
           
            <div class="row-us3b1m">
                <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Lubov Kovalyova</h2>
                <div class="Pb"><p>Strategy, Branding</p></div>
                <hr>
                <p>Work experience: 15 years</p>
                <p>Santa-Bremor Ltd.</p>
                <p>Santa-Rest Ltd.</p>
                <p> STiM Ltd.</p>
                <p>I may assist you in Russian</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 296 42 90 80</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>lubov.kovaleva@gid.company</h3></td>  
                </tr>
                </table>
            </div>

             <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Ekaterina Zarkova</h2>
                <div class="Pb"><p>Branding, Project Management</p></div>
                <hr>
                <p>Work experience: 8 years</p>
                <p>STiM Ltd.</p>
                <p>Transconsult Ltd.</p>
                <p>VRP Consulting Ltd.</p>
                <p>I may assist you in Russian, English</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 297 980 626</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                </tr>
                </table>
            </div>

        </div>   
           
    

         <div class="row-us3b1m">            
             <div class="persona">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Irina Turanskaya</h2>
                <div class="Pb"><p>Clienting, Analytics, Sales</p></div>
                <hr>
                <p>Belfakta Media Ltd. team leader,</p>
                <p>STiM Ltd.marketing specialist</p>
                <p>Head of educational center "First step"</p>
                <p>I may assist you in Russian, English, Polish</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 29 723 40 25</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>irina.turanskaya@gid.company</h3></td>  
                </tr>
                </table>
            </div>

              <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Dmitry Kovalyov</h2>
                <div class="Pb"><p>Project Management, Sales</p></div>
                <hr>
                <p>work experience 15 years at</p>
                <p>construction materials sales and transport services</p>
                <p>I may assist you in Russian, Polish</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+48 573 913 300</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>dmitry.kovalev@gid.company</h3></td>  
                </tr>
                </table>
            </div>
         
        </div>

        <!-- m1 -->


            <div class="row-us3b1m1">
                <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Lubov Kovalyova</h2>
                <div class="Pb"><p>Strategy, Branding</p></div>
                <hr>
                <p>Work experience: 15 years</p>
                <p>Santa-Bremor Ltd.</p>
                <p>Santa-Rest Ltd.</p>
                <p> STiM Ltd.</p>
                <p>I may assist you in Russian</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 296 42 90 80</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>lubov.kovaleva@gid.company</h3></td>  
                </tr>
                </table>
            </div>

        </div>

         <div class="row-us3b1m1">

             <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Ekaterina Zarkova</h2>
                <div class="Pb"><p>Branding, Project Management</p></div>
                <hr>
                <p>Work experience: 8 years</p>
                <p>STiM Ltd.</p>
                <p>Transconsult Ltd.</p>
                <p>VRP Consulting Ltd.</p>
                <p>I may assist you in Russian, English</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 297 980 626</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                </tr>
                </table>
            </div>

        </div>         
             
            <div class="row-us3b1m1">

              <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Dmitry Kovalyov</h2>
                <div class="Pb"><p>Project Management, Sales</p></div>
                <hr>
                <p>work experience 15 years at</p>
                <p>construction materials sales and transport services</p>
                <p>I may assist you in Russian, Polish</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+48 573 913 300</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>dmitry.kovalev@gid.company</h3></td>  
                </tr>
                </table>
            </div>
         
        </div>

         <div class="row-us3b1m1">   
            <div class="persona">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Irina Turanskaya</h2>
                <div class="Pb"><p>Clienting, Analytics, Sales</p></div>
                <hr>
                <p>Belfakta Media Ltd. team leader,</p>
                <p>STiM Ltd.marketing specialist</p>
                <p>Head of educational center "First step"</p>
                <p>I may assist you in Russian, English, Polish</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 29 723 40 25</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>irina.turanskaya@gid.company</h3></td>  
                </tr>
                </table>
            </div>
        </div>


    </div>    

<div class="block-C ">

		<div class="Contacts-blockm">
			<div class="contacts-menu-m">
				<h1>Contacts</h1>
			</div>

            <section id="contacts" ></section>

            <div class="contacts-menu">
                <h1>Contacts</h1>
                <table>
                <tr>
                <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 723 40 25 </h2></td>     
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 535 594 761  </h2></td>  
                </tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 680 40 40</h2></td>  
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 515 952 543</h2></td>  
                </tr>                
                <tr>
                <tr>
                <td class="mail"><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                <td><h2>sales@gid.company</h2></td>  
                </tr>
                </table>
            </div>

			<div class="contacts-menu1-m">
				<div class="contactpost-m">
            		<form action="/en/contact" method="post">
                		<div class="contactpost1">                    
                        	<p><input type="text" class="form-control" name="name" placeholder="Name"></p>                  
                		</div>
                		<div class="contactpost2">             
                        	<p><input type="text" class="form-control" name="email" placeholder="E-mail"></p>
                		</div>
                		<div class="contactpost3">                    
                        	<p><textarea rows="5" class="form-control" name="text" placeholder="Message"></textarea></p>
                		</div>

                        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                		<br>
                		<div id="success">
                    		<button type="submit" class="btn btn-secondary" id="sendMessageButton">Contact us</button>     
                		</div>         
            		</form>
        		</div>
			</div>

                <div class="contacts-menu-m1">
                    <table>
                        <tr>
                            <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 723 40 25 </h2></td><td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 535 594 761  </h2></td>      
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 680 40 40</h2></td><td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 515 952 543</h2></td>   
                        </tr>
                        <tr>

                        </tr>   

                    </table>
                    <table align="center">
                        <tr >
                            <td class="mail" ><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                            <td><h2>sales@gid.company</h2></td>  
                        </tr>
                    </table> 
                </div>
				
		</div>

	</div>

    </div>  
        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>

</div>