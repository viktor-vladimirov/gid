
    <div class="block-A">

        <div class="menu">
        <a class="menu-left" href="/pl"></a>

        <div class="dropdown">
              <button class="mainmenubtn"></button>
              <div class="dropdown-child">
                <div class="drop">
                <a href="/pl" class="close"></a>
                </div>
                <div class="dropr1">                
                <a href="/" class="launge1">ru</a>
                <a href="/pl" class="launge2">pl</a>
                <a href="/en" class="launge3">en</a>
                </div>
                <div class="dropr2">
                <a href="/pl/projects">Projekty</a>
                <a href="/pl/aboutus">O nas</a>
                <a href="/pl/contact">Kontakt</a>
                </div>
              </div>
        </div>

        <div class="menu-right">
            <a href="/pl">pl</a>
            <a href="/en">en</a>
            <a href="/">ru</a>
        </div>
        </div>
        <div class="menu-right1">
            <a href="/pl/posts">Projekty</a>
            <a href="/pl/aboutus">O nas</a>
            <a href="/pl/#contacts">Kontakt</a>
        </div>
        <div class="menu1">
            <p>Projekty</p>
            <hr>
            <h1>GID - skuteczny rozwój produktu do rozwiązywania problemów naszych klientów w zakresie promocji</h1>
        </div>
        <div class="menu2">
            <a href="/pl/#strategy">Strategia</a>
            <a href="/pl/#analytics">Analityka</a>
            <a href="/pl/#branding">Branding</a>
            <a href="/pl/#klienting">Clienting</a>
        </div>
    </div>
    
<!-- # -->
<div class="Posts-list">
            <?php if (empty($vars['list'])): ?>
                    <p>empty list</p>
            <?php else: ?>
                <?php foreach ($vars['list'] as $val): ?>

                    <div class="Posts-wrapper">
                        <a href="/post/<?php echo $val['id']; ?>">
                            <div class="Posts-container" style="background-image: url('/public/materials/300x167/<?php echo $val['id']; ?>.jpg')">
                            </div>
                            <div class="subtitlecontainer">
                                <h2 class="post-subtitle"><?php echo htmlspecialchars($val['description'], ENT_QUOTES); ?></h2>
                            </div>
                            <h5 class="post-title"><?php echo htmlspecialchars($val['name'], ENT_QUOTES); ?></h5> 
                        </a>
                    </div>

                <?php endforeach; ?>
            <?php endif; ?>     
</div>
<!-- # -->

 

        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>
