<div class="block-A">

        <div class="menu">
        <a class="menu-left" href="/pl"></a>

        <div class="dropdown">
              <button class="mainmenubtn"></button>
              <div class="dropdown-child">
                <div class="drop">
                <a href="/pl" class="close"></a>
                </div>
                <div class="dropr1">                
                <a href="/" class="launge1">ru</a>
                <a href="/pl" class="launge2">pl</a>
                <a href="/en" class="launge3">en</a>
                </div>
                <div class="dropr2">
                <a href="/pl/projects">Projekty</a>
                <a href="/pl/aboutus">O nas</a>
                <a href="/pl/contact">Kontakt</a>
                </div>
              </div>
        </div>

        <div class="menu-right">
            <a href="/pl">pl</a>
            <a href="/en">en</a>
            <a href="/">ru</a>
        </div>
        </div>
        <div class="menu-right1">
            <a href="/pl/posts">Projekty</a>
            <a href="/pl/aboutus">O nas</a>
            <a href="#contacts">Kontakt</a>
        </div>
        <div class="menu1">
            <p>Projekty</p>
            <hr>
            <h1>GID - skuteczny rozwój produktu do rozwiązywania problemów naszych klientów w zakresie promocji</h1>
        </div>
        <div class="menu2">
            <a href="/pl/#strategy">Strategia</a>
            <a href="/pl/#analytics">Analityka</a>
            <a href="/pl/#branding">Branding</a>
            <a href="/pl/#klienting">Clienting</a>
        </div>
</div>

            <div class="row2m">
            <div class="row2Leftm">
            <section id="projects" ><h1>Projekty</h1></section>
            <hr>
            </div>
            </div>


<!--Тус блок с постами -->

          <div class="container2">       
         
            <?php if (empty($vars['list'])): ?>
                <p>Список Новостей пуст</p>
            <?php else: ?>
                <?php foreach ($vars['list'] as $val): ?>
                    
                    <div class="wrapperpost">
                        <a href="/pl/post/<?php echo $val['id']; ?>">
                        <div class="postcontainer" style="background-image: url('/public/materials/300x167/<?php echo $val['id']; ?>.jpg')">
                        </div>
                        <div class="subtitlecontainer">
                        <h2 class="post-subtitle"><?php echo htmlspecialchars($val['description'], ENT_QUOTES); ?></h2>
                        </div>
                        <h5 class="post-title"><?php echo htmlspecialchars($val['name'], ENT_QUOTES); ?></h5> 
                        </a>
                    </div>

                <?php endforeach; ?>
       
        <div class="postcontainer1">
                   <?php echo $vars['pagination'] ?>
        </div>
            <?php endif; ?>     
        </div>


         <!--Тус блок с постами -->


<div class="block-C ">

		<div class="Contacts-blockm">
			<div class="contacts-menu-m">
				<h1>Kontakt</h1>
			</div>

            <section id="contacts" ></section>

            <div class="contacts-menu">
                <h1>Kontakt</h1>
                <table>
                <tr>
                <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 723 40 25 </h2></td>     
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 535 594 761  </h2></td>  
                </tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 680 40 40</h2></td>  
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 515 952 543</h2></td>  
                </tr>                
                <tr>
                <tr>
                <td class="mail"><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                <td><h2>sales@gid.company</h2></td>  
                </tr>
                </table>
            </div>

			<div class="contacts-menu1-m">
				<div class="contactpost-m">
            		<form action="/pl/contact" method="post">
                		<div class="contactpost1">                    
                        	<p><input type="text" class="form-control" name="name" placeholder="Imię"></p>                  
                		</div>
                		<div class="contactpost2">             
                        	<p><input type="text" class="form-control" name="email" placeholder="E-mail"></p>
                		</div>
                		<div class="contactpost3">                    
                        	<p><textarea rows="5" class="form-control" name="text" placeholder="Wiadomość"></textarea></p>
                		</div>

                        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                		<br>
                		<div id="success">
                    		<button type="submit" class="btn btn-secondary" id="sendMessageButton">Kontakt</button>     
                		</div>         
            		</form>
        		</div>
			</div>

                <div class="contacts-menu-m1">
                    <table>
                        <tr>
                            <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 723 40 25 </h2></td><td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 535 594 761  </h2></td>      
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 680 40 40</h2></td><td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 515 952 543</h2></td>   
                        </tr>
                        <tr>

                        </tr>   

                    </table>
                    <table align="center">
                        <tr >
                            <td class="mail" ><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                            <td><h2>sales@gid.company</h2></td>  
                        </tr>
                    </table> 
                </div>
				
		</div>

	</div>

        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>

</div>