<div class="wrapper">

    <div class="block-A">

        <div class="menu">
        <a class="menu-left" href="/pl"></a>

        <div class="dropdown">
              <button class="mainmenubtn"></button>
              <div class="dropdown-child">
                <div class="drop">
                <a href="/pl" class="close"></a>
                </div>
                <div class="dropr1">                
                <a href="/" class="launge1">ru</a>
                <a href="/pl" class="launge2">pl</a>
                <a href="/en" class="launge3">en</a>
                </div>
                <div class="dropr2">
                <a href="/pl/projects">Projekty</a>
                <a href="/pl/aboutus">O nas</a>
                <a href="/pl/contact">Kontakt</a>
                </div>
              </div>
        </div>

        <div class="menu-right">
            <a href="/pl">pl</a>
            <a href="/en">en</a>
            <a href="/">ru</a>
        </div>
        </div>
        <div class="menu-right1">
            <a href="/pl/posts">Projekty</a>
            <a href="/pl/aboutus">O nas</a>
            <a href="#contacts">Kontakt</a>
        </div>
        <div class="menu1">
            <p>Pełny zakres usług marketingowych</p>
            <hr>
            <h1>GID - skuteczny rozwój produktu do rozwiązywania problemów naszych klientów w zakresie promocji</h1>
        </div>
        <div class="menu2">
            <a href="#strategy">Strategia</a>
            <a href="#analytics">Analityka</a>
            <a href="#branding">Branding</a>
            <a href="#klienting">Clienting</a>
        </div>


    </div>

    <!-- ###### BLOK B ####### -->

    <div class="block-B">
        
        <div class="row1"> 
          <table>
            <tr>
            <td class="tb1"><img src="/public/images/clienci.png" alt="img" width="75" height="75"></td>
            <td class="tittlet">Wiemy, gdzie sąTwoi klienci</td>
           
       
                        
            <td><img src="/public/images/decyzje.png" alt="img" width="65" height="75"></td>
            <td class="tittlet">Podejmujemy decyzje</td>
            
      
            <td><img src="/public/images/ceny.png" alt="img" width="70" height="75"></td>
            <td class="tittlet">Oferujemy konkurencyjne ceny</td>
            </tr>
          </table>
        </div>


        <div class="row1m"> 

          <table>                      
            <td><img src="/public/images/decyzje.png" alt="img" width="67" height="75"></td>
            <td class="tittlet">Podejmujemy decyzje</td>
          </table>  

          <table>
            <tr>
            <td><img src="/public/images/clienci.png" alt="img" width="75" height="75"></td>
            <td class="tittlet">Wiemy, gdzie sąTwoi klienci</td>
          </table>
       
      
          <table>   
            <td><img src="/public/images/ceny.png" alt="img" width="70" height="75"></td>
            <td class="tittlet">Oferujemy konkurencyjne ceny</td>
            </tr>
          </table>
        </div>


                   <!-- Блок дублируемый -2-3-4-5- -->

          <!-- ** СТРАТЕГИЯ ** -->

        <div class="row2">
            <div class="row2Left">
            <section id="strategy" ><h1>Strategia</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Zastanawiasz się, gdzie rozwijać biznes </p>
                <p>Bierzesz pod uwagę nowe rynki</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
            <section id="strategy" ><h1>Strategia</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1m">
                <p>Zastanawiasz się, gdzie rozwijać biznes 
                Bierzesz pod uwagę nowe rynki</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Zaproponujemy skuteczne rozwiązanie
                 oparte na problemie, oparte na analizie</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Zaproponujemy skuteczne rozwiązanie </p>
                <p>oparte na problemie, oparte na analizie</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>Analiza możliwych strategii sprzedaży</li>
                <li>Opracowanie hipotez dotyczących promocji i wejścia na nowe rynk</li>
                <li>Propozycje nowych produktów i kanałów promocyjnych</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>Analiza możliwych strategii sprzedaży</li>
                <li>Opracowanie hipotez dotyczących promocji i wejścia na nowe rynk</li>
                <li>Propozycje nowych produktów i kanałów promocyjnych</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Potrzebujesz strategii?</a>
        </div>


        <!-- ** АНАЛИТИКА ** -->


        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
            <section id="analytics" ><h1>Analityka</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Planujesz wejść na nowe rynki</p>
                <p>Opracowujecie strategii promocj</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
             <section id="analytics" ><h1>Analityka</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1m">
                <p>Planujesz wejść na nowe rynki
                Opracowujecie strategii promocj</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Potwierdzimy Twoje hipotezy liczbami.</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Potwierdzimy Twoje hipotezy liczbami.</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>Określamy cel badania</li>
                <li>Wybieramy techniki i narzędzia do Państwa zadania</li>
                <li>Zbieramy i analizujemy informacje</li>
                <li>Przedstawiamy raport w wygodnej Państwu formie z wnioskami i poleceniami</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>Określamy cel badania</li>
                <li>Wybieramy techniki i narzędzia do Państwa zadania</li>
                <li>Zbieramy i analizujemy informacje</li>
                <li>Przedstawiamy raport w wygodnej Państwu formie z wnioskami i poleceniami</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Szukasz wniosków?</a>
        </div>

      <!-- ** АНАЛИТИКА ** -->
        
       <!--# Блок дублируемый -2-3-4-5--->

        <!-- ** БРЕНДИНГ ** -->


        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
            <section id="branding" ><h1>Branding</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Masz nowy dobry produkt</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
            <section id="branding" ><h1>Branding</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1mBr">
                <p>Masz nowy dobry produkt</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Wiemy, jak ładnie „zapakować” </p>
                <p>i dobrać nazwę!</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Wiemy, jak ładnie „zapakować” </p>
                <p>i dobrać nazwę!</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>Przeprowadzamy audyt marki i opracowujemy propozycje (hipotezy) dotyczące jej promocji</li>
                <li>Rozwijamy strategię pozycjonowania i platformę marki</li>
                <li>Tworzymy strategię komunikacji marki.</li>
                <li>Dobieramy najlepsze kanały promocji</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>Przeprowadzamy audyt marki i opracowujemy propozycje (hipotezy) dotyczące jej promocji</li>
                <li>Rozwijamy strategię pozycjonowania i platformę marki</li>
                <li>Tworzymy strategię komunikacji marki.</li>
                <li>Dobieramy najlepsze kanały promocji</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Szukasz idei?</a>
        </div>

      <!-- ** КЛИЕНТИНГ ** -->



        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
            <section id="klienting" ><h1>Clienting</h1></section>
            <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Poszukujesz nowych punktów interakcji z klientem</p>
            </div>
        </div>

          <div class="row2m">
            <div class="row2Leftm">
            <section id="klienting" ><h1>Clienting</h1></section>
            <hr>
            </div>
            <div class="row2f">
            <div class="row2Rightm">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1mBr">
                <p>Poszukujesz nowych punktów interakcji z klientem</p>
            </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Jesteśmy w stanie dostosować i przekazać 
                Twoją ofertę każdemu klientowi!</p> 
 
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Jesteśmy w stanie dostosować i przekazać </p>         
                <p>Twoją ofertę każdemu klientowi!</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
            <ul>
                <li>Analizujemy portret klienta, określamy jego potrzeby</li>
                <li>Oferujemy najbardziej efektywne „punkty dotykowe” klientów</li>
                <li>Develop communication strategy</li>
                <li>Opracowujemy bariery zatrzymywania klientów</li>
            </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
            <ul>
                <li>Analizujemy portret klienta, określamy jego potrzeby</li>
                <li>Oferujemy najbardziej efektywne „punkty dotykowe” klientów</li>
                <li>Opracowujemy strategię komunikacji</li>
                <li>Opracowujemy bariery zatrzymywania klientów</li>
            </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Szukasz klientów?</a>
        </div>

        <!-- ** Проекты ** -->

        <div class="row2">
            <div class="row2Left">
            <section id="projects" ><h1>Projekty</h1></section>
            <hr>
            </div>
        </div>

        <div class="row2m">
            <div class="row2Leftm">
            <section id="projects" ><h1>Projekty</h1></section>
            <hr>
            </div>
        </div>


         <!--Тус блок с постами -->

          <div class="container2">       
         
            <?php if (empty($vars['list'])): ?>
                <p>Список Новостей пуст</p>
            <?php else: ?>
                <?php foreach ($vars['list'] as $val): ?>
                    
                    <div class="wrapperpost">
                        <a href="/pl/post/<?php echo $val['id']; ?>">
                        <div class="postcontainer" style="background-image: url('/public/materials/300x167/<?php echo $val['id']; ?>.jpg')">
                        </div>
                        <div class="subtitlecontainer">
                        <h2 class="post-subtitle"><?php echo htmlspecialchars($val['description'], ENT_QUOTES); ?></h2>
                        </div>
                        <h5 class="post-title"><?php echo htmlspecialchars($val['name'], ENT_QUOTES); ?></h5> 
                        </a>
                    </div>

                <?php endforeach; ?>
       
        <div class="row5">
            <a href="<?php echo '/pl/posts';?>">Wszystkie projekty</a>
        </div>
            <?php endif; ?>     
        </div>


         <!--Тус блок с постами -->


<div class="block-C ">

		<div class="Contacts-blockm">
			<div class="contacts-menu-m">
				<h1>Kontakt</h1>
			</div>

            <section id="contacts" ></section>

            <div class="contacts-menu">
                <h1>Kontakt</h1>
                <table>
                <tr>
                <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 723 40 25 </h2></td>     
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 535 594 761  </h2></td>  
                </tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 680 40 40</h2></td>  
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 515 952 543</h2></td>  
                </tr>                
                <tr>
                <tr>
                <td class="mail"><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                <td><h2>sales@gid.company</h2></td>  
                </tr>
                </table>
            </div>

			<div class="contacts-menu1-m">
				<div class="contactpost-m">
            		<form action="/pl/contact" method="post">
                		<div class="contactpost1">                    
                        	<p><input type="text" class="form-control" name="name" placeholder="Imię"></p>                  
                		</div>
                		<div class="contactpost2">             
                        	<p><input type="text" class="form-control" name="email" placeholder="E-mail"></p>
                		</div>
                		<div class="contactpost3">                    
                        	<p><textarea rows="5" class="form-control" name="text" placeholder="Wiadomość"></textarea></p>
                		</div>

                        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                		<br>
                		<div id="success">
                    		<button type="submit" class="btn btn-secondary" id="sendMessageButton">Kontakt</button>     
                		</div>         
            		</form>
        		</div>
			</div>

                <div class="contacts-menu-m1">
                    <table>
                        <tr>
                            <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 723 40 25 </h2></td><td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 535 594 761  </h2></td>      
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 680 40 40</h2></td><td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 515 952 543</h2></td>   
                        </tr>
                        <tr>

                        </tr>   

                    </table>
                    <table align="center">
                        <tr >
                            <td class="mail" ><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                            <td><h2>sales@gid.company</h2></td>  
                        </tr>
                    </table> 
                </div>
				
		</div>

	</div>

        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>

</div>