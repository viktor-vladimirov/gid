<div class="wrapper">


    <div class="block-A">

        <div class="menu">
        <a class="menu-left" href="/pl"></a>

        <div class="dropdown">
              <button class="mainmenubtn"></button>
              <div class="dropdown-child">
                <div class="drop">
                <a href="/pl" class="close"></a>
                </div>
                <div class="dropr1">                
                <a href="/" class="launge1">ru</a>
                <a href="/pl" class="launge2">pl</a>
                <a href="/en" class="launge3">en</a>
                </div>
                <div class="dropr2">
                <a href="/pl/projects">Projekty</a>
                <a href="/pl/aboutus">O nas</a>
                <a href="/pl/contact">Kontakt</a>
                </div>
              </div>
        </div>

        <div class="menu-right">
            <a href="/pl">pl</a>
            <a href="/en">en</a>
            <a href="/">ru</a>
        </div>
        </div>
        <div class="menu-right1">
            <a href="/pl/posts">Projekty</a>
            <a href="/pl/aboutus">O nas</a>
            <a href="#contacts">Kontakt</a>
        </div>
        <div class="menu1">
            <p>O nas</p>
            <hr>
            <h1>GID - skuteczny rozwój produktu do rozwiązywania problemów naszych klientów w zakresie promocji</h1>
        </div>
        <div class="menu2">
            <a href="/pl/#strategy">Strategia</a>
            <a href="/pl/#analytics">Analityka</a>
            <a href="/pl/#branding">Branding</a>
            <a href="/pl/#klienting">Clienting</a>
        </div>
    </div>

    <div class="block-D">

        <div class="row-us1">
            <h1>O nas</h1>
            <hr>
        </div>

        <div class="row-us2">
            <text>
            Jeżeli potrzebujesz zbadać rynek, przeanalizować
            działalność konkurentów, zidentyfikować albo stworzyć przewagę konkurencyjną -
            nasz zespół GID Consulting pomoże Ci zrealizować kompleksowe projekty marketingowe. Jeżeli planujesz wejść na rynek WNP, jesteśmy gotowi zaoferować strategie
            promocji, zbudować lub adaptować markę, stworzyć opakowanie.
            Nasz zespół - profesjonaliści z doświadczeniem w marketingu i
            sprzedaży w dużych przedsiębiorstwach handlowych i przemysłowych na Białorusi.
            Dzisiaj chcemy Ci zaoferować
            </text>
        </div>

        <div class="row-us1">
            <h1>GID Team</h1>
            <hr>
        </div>

        <div class="row-us3b1">
            <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Lubov Kovalyova</h2>
                <div class="Pb"><p>Strategy, Branding</p></div>
                <hr>
                <p>Doświadczenie zawodowe: 15 lat</p>
                <p>Santa-Bremor Ltd.</p>
                <p>Santa-Rest Ltd.</p>
                <p>STiM Ltd.marketing specialist</p>
                <p>Pomogę w języku rosyjskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 296 42 90 80</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>lubov.kovaleva@gid.company</h3></td>  
                </tr>
                </table>
            </div>

             <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Ekaterina Zarkova</h2>
                <div class="Pb"><p>Branding, Project Management</p></div>
                <hr>
                <p>Work experience: 8 years</p>
                <p>STiM Ltd.</p>
                <p>Transconsult Ltd.</p>
                <p>VRP Consulting Ltd.</p>
                <p>Pomogę w języku rosyjskim i angielskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 297 980 626</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                </tr>
                </table>
            </div>

             <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Dmitry Kovalyov</h2>
                <div class="Pb"><p>Project Management, Sales</p></div>
                <hr>
                <p>Doświadczenie zawodowe:</p> 
                <p>15 lat w sprzedaży
                materiałów budowlanych i usług transportowych</p>
                <p>Pomogę w języku rosyjskim i polskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+48 573 913 300</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>dmitry.kovalev@gid.company</h3></td>  
                </tr>
                </table>
            </div>
        </div>

        <div class="row-us3b2">
            <div class="persona1">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Irina Turanskaya</h2>
                <div><h4>Clienting, Analytics, Sales</h4></div>
                <hr>
                <p>Doświadczenie zawodowe: 15 lat</p>
                <p>Belfakta Media Ltd. team leader,</p>
                <p>STiM Ltd.marketing specialist</p>
                <p>Head of educational center "First step"</p>
                <p>Pomogę w języku rosyjskim, polskim i angielskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 29 723 40 25</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>irina.turanskaya@gid.company</h3></td>  
                </tr>
                </table>
            </div>    
         </div>   

        <!-- m -->
           
            <div class="row-us3b1m">
                <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Lubov Kovalyova</h2>
                <div class="Pb"><p>Strategy, Branding</p></div>
                <hr>
                <p>Doświadczenie zawodowe: 15 lat</p>
                <p>Santa-Bremor Ltd.</p>
                <p>Santa-Rest Ltd.</p>
                <p>STiM Ltd.marketing specialist</p>
                <p>Pomogę w języku rosyjskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 296 42 90 80</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>lubov.kovaleva@gid.company</h3></td>  
                </tr>
                </table>
            </div>

             <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Ekaterina Zarkova</h2>
                <div class="Pb"><p>Branding, Project Management</p></div>
                <hr>
                <p>Work experience: 8 years</p>
                <p>STiM Ltd.</p>
                <p>Transconsult Ltd.</p>
                <p>VRP Consulting Ltd.</p>
                <p>Pomogę w języku rosyjskim i angielskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 297 980 626</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                </tr>
                </table>
            </div>

        </div>   
           
    

         <div class="row-us3b1m">            
             <div class="persona">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Irina Turanskaya</h2>
                <div class="Pb"><p>Clienting, Analytics, Sales</p></div>
                <hr>
                <p>Doświadczenie zawodowe: 15 lat</p>
                <p>Belfakta Media Ltd. team leader,STiM Ltd.marketing specialist</p>
                <p>Head of educational center "First step"</p>
                <p>Pomogę w języku rosyjskim, polskim i angielskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 29 723 40 25</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>irina.turanskaya@gid.company</h3></td>  
                </tr>
                </table>
            </div>

              <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Dmitry Kovalyov</h2>
                <div class="Pb"><p>Project Management, Sales</p></div>
                <hr>
                <p>Doświadczenie zawodowe:</p> 
                <p>15 lat w sprzedaży
                materiałów budowlanych i usług transportowych</p>
                <p>Pomogę w języku rosyjskim i polskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+48 573 913 300</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>dmitry.kovalev@gid.company</h3></td>  
                </tr>
                </table>
            </div>
         
        </div>

        <!-- m1 -->


            <div class="row-us3b1m1">
                <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Lubov Kovalyova</h2>
                <div class="Pb"><p>Strategy, Branding</p></div>
                <hr>
                <p>Doświadczenie zawodowe: 15 lat</p>
                <p>Santa-Bremor Ltd.</p>
                <p>Santa-Rest Ltd.</p>
                <p>STiM Ltd.marketing specialist</p>
                <p>Pomogę w języku rosyjskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 296 42 90 80</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>lubov.kovaleva@gid.company</h3></td>  
                </tr>
                </table>
            </div>

        </div>

         <div class="row-us3b1m1">

             <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Ekaterina Zarkova</h2>
                <div class="Pb"><p>Branding, Project Management</p></div>
                <hr>
                <p>Work experience: 8 years</p>
                <p>STiM Ltd.</p>
                <p>Transconsult Ltd.</p>
                <p>VRP Consulting Ltd.</p>
                <p>Pomogę w języku rosyjskim i angielskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 297 980 626</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                </tr>
                </table>
            </div>

        </div>         
             
            <div class="row-us3b1m1">

              <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Dmitry Kovalyov</h2>
                <div class="Pb"><p>Project Management, Sales</p></div>
                <hr>
                <p>Doświadczenie zawodowe:</p> 
                <p>15 lat w sprzedaży
                materiałów budowlanych i usług transportowych</p>
                <p>Pomogę w języku rosyjskim i polskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+48 573 913 300</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>dmitry.kovalev@gid.company</h3></td>  
                </tr>
                </table>
            </div>
         
        </div>

         <div class="row-us3b1m1">   
            <div class="persona">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Irina Turanskaya</h2>
                <div class="Pb"><p>Clienting, Analytics, Sales</p></div>
                <hr>
                <p>Doświadczenie zawodowe: 15 lat</p>
                <p>Belfakta Media Ltd. team leader,STiM Ltd.marketing specialist</p>
                <p>Head of educational center "First step"</p>
                <p>Pomogę w języku rosyjskim, polskim i angielskim</p>
                <hr>
                <table>
                <tr>
                <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                <td><h3>+375 29 723 40 25</h3></td>     
                </tr>
                <tr>
                <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                <td><h3>irina.turanskaya@gid.company</h3></td>  
                </tr>
                </table>
            </div>
        </div>


    </div>    

<div class="block-C ">

		<div class="Contacts-blockm">
			<div class="contacts-menu-m">
				<h1>Kontakt</h1>
			</div>

            <section id="contacts" ></section>

            <div class="contacts-menu">
                <h1>Kontakt</h1>
                <table>
                <tr>
                <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 723 40 25 </h2></td>     
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 535 594 761  </h2></td>  
                </tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 680 40 40</h2></td>  
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                <td><h2>+48 515 952 543</h2></td>  
                </tr>                
                <tr>
                <tr>
                <td class="mail"><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                <td><h2>sales@gid.company</h2></td>  
                </tr>
                </table>
            </div>

			<div class="contacts-menu1-m">
				<div class="contactpost-m">
            		<form action="/pl/contact" method="post">
                		<div class="contactpost1">                    
                        	<p><input type="text" class="form-control" name="name" placeholder="Imię"></p>                  
                		</div>
                		<div class="contactpost2">             
                        	<p><input type="text" class="form-control" name="email" placeholder="E-mail"></p>
                		</div>
                		<div class="contactpost3">                    
                        	<p><textarea rows="5" class="form-control" name="text" placeholder="Wiadomość"></textarea></p>
                		</div>

                        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                		<br>
                		<div id="success">
                    		<button type="submit" class="btn btn-secondary" id="sendMessageButton">Kontakt</button>     
                		</div>         
            		</form>
        		</div>
			</div>

                <div class="contacts-menu-m1">
                    <table>
                        <tr>
                            <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 723 40 25 </h2></td><td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 535 594 761  </h2></td>      
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 680 40 40</h2></td><td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 515 952 543</h2></td>   
                        </tr>
                        <tr>

                        </tr>   

                    </table>
                    <table align="center">
                        <tr >
                            <td class="mail" ><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                            <td><h2>sales@gid.company</h2></td>  
                        </tr>
                    </table> 
                </div>
				
		</div>

	</div>

        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>

</div>