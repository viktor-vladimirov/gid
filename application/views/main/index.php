<div class="wrapper">

    <div class="block-A">

        <div class="menu">
            <a class="menu-left" href="/"></a>

            <div class="dropdown">
                <button class="mainmenubtn"></button>
                <div class="dropdown-child">
                    <div class="drop">
                        <a href="/" class="close"></a>
                    </div>
                    <div class="dropr1">                
                        <a href="/" class="launge1">ru</a>
                        <a href="/pl" class="launge2">pl</a>
                        <a href="/en" class="launge3">en</a>
                    </div>
                    <div class="dropr2">
                        <a href="/projects">Проекты</a>
                        <a href="/aboutus">О нас</a>
                        <a href="/contact">Контакты</a>
                    </div>
                </div>
            </div>

            <div class="menu-right">
                <a href="/pl">pl</a>
                <a href="/en">en</a>
                <a href="/">ru</a>
            </div>
        </div>
        <div class="menu-right1">
            <a href="/posts">Проекты</a>
            <a href="/aboutus">О нас</a>
            <a href="/#contacts">Контакты</a>
        </div>
        <div class="menu1">
            <p>Весь спектр маркетинговых услуг</p>
            <hr>
          <!--  <h1>GID - это разработка эффективного продукта для
                решения задач наших клиентов в продвижении.</h1>
          -->
        </div>
        <div class="menu2">
            <a href="#strategy">Стратегия</a>
            <a href="#analytics">Аналитика</a>
            <a href="#branding">Брендинг</a>
            <a href="#klienting">Клиентинг</a>
        </div>


    </div>

    <!-- ###### BLOK B ####### -->

    <div class="block-B">

        <div class="row1"> 
            <table>
                <tr>
                    <td class="tb1"><img src="/public/images/clienci.png" alt="img" width="75" height="75"></td>
                    <td class="tittlet">Знаем, где найти клиентов</td>



                    <td><img src="/public/images/decyzje.png" alt="img" width="65" height="75"></td>
                    <td class="tittlet">Принимаем решения</td>


                    <td><img src="/public/images/ceny.png" alt="img" width="70" height="75"></td>
                    <td class="tittlet">Предлагаем конкурентные цены</td>
                </tr>
            </table>
        </div>


        <div class="row1m"> 

            <table>                      
                <td><img src="/public/images/decyzje.png" alt="img" width="67" height="75"></td>
                <td class="tittlet">Принимаем решения</td>
            </table>  

            <table>
                <tr>
                    <td><img src="/public/images/clienci.png" alt="img" width="75" height="75"></td>
                    <td class="tittlet">Знаем, где найти клиентов</td>
            </table>


            <table>   
                <td><img src="/public/images/ceny.png" alt="img" width="70" height="75"></td>
                <td class="tittlet">Предлагаем конкурентные цены</td>
                </tr>
            </table>
        </div>


        <!-- Блок дублируемый -2-3-4-5- -->

        <!-- ** СТРАТЕГИЯ ** -->

        <div class="row2">
            <div class="row2Left">
                <section id="strategy" ><h1>Стратегия</h1></section>
                <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Задумались куда развивать бизнес</p>
                <p>Рассматриваете новые рынки</p>
            </div>
        </div>

        <div class="row2m">
            <div class="row2Leftm">
                <section id="strategy" ><h1>Стратегия</h1></section>
                <hr>
            </div>
            <div class="row2f">
                <div class="row2Rightm">
                    <img src="/public/images/question.png" alt="img" width="40" height="60">        
                </div>
                <div class="row2Right1m">
                    <p>Задумались куда развивать бизнес Рассматриваете новые рынки</p>
                </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Мы предложим эффективное решение,
                    исходя из задачи, основываясь на аналитике .</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Мы предложим эффективное решение,</p>
                <p>исходя из задачи, основываясь на аналитике .</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
                <ul>
                    <li>Анализ возможных стратегий продаж</li>
                    <li>Разработка гипотез продвижения и выхода на новые рынки </li>
                    <li>Предложения по новым продуктам и каналам продвижения</li>
                </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
                <ul>
                    <li>Анализ возможных стратегий продаж</li>
                    <li>Разработка гипотез продвижения и выхода на новые рынки </li>
                    <li>Предложения по новым продуктам и каналам продвижения</li>
                </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Нужна стратегия?</a>
        </div>


        <!-- ** АНАЛИТИКА ** -->


        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
                <section id="analytics" ><h1>Аналитика</h1></section>
                <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Планируете выход на новые рынки </p>
                <p>Разрабатываете стратегии продвижения</p>
            </div>
        </div>

        <div class="row2m">
            <div class="row2Leftm">
                <section id="analytics" ><h1>Аналитика</h1></section>
                <hr>
            </div>
            <div class="row2f">
                <div class="row2Rightm">
                    <img src="/public/images/question.png" alt="img" width="40" height="60">        
                </div>
                <div class="row2Right1m">
                    <p>Планируете выход на новые рынки Разрабатываете стратегии продвижения</p>
                </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Мы подтвердим цифрами ваши гипотезы</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Мы подтвердим цифрами ваши гипотезы</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
                <ul>
                    <li>Определяем цель исследования</li>
                    <li>Выбираем методики и инструменты под вашу задачу </li>
                    <li>Собираем и анализируем информацию</li>
                    <li>Предоставляем отчет в удобной вам форме с выводами и рекомендациями</li>
                </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
                <ul>
                    <li>Определяем цель исследования</li>
                    <li>Выбираем методики и инструменты под вашу задачу </li>
                    <li>Собираем и анализируем информацию</li>
                    <li>Предоставляем отчет в удобной вам форме с выводами и рекомендациями</li>
                </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Нужны выводы?</a>
        </div>

        <!-- ** АНАЛИТИКА ** -->

        <!--# Блок дублируемый -2-3-4-5--->

        <!-- ** БРЕНДИНГ ** -->


        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
                <section id="branding" ><h1>Брендинг</h1></section>
                <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>У вас есть новый хороший продукт</p>
            </div>
        </div>

        <div class="row2m">
            <div class="row2Leftm">
                <section id="branding" ><h1>Брендинг</h1></section>
                <hr>
            </div>
            <div class="row2f">
                <div class="row2Rightm">
                    <img src="/public/images/question.png" alt="img" width="40" height="60">        
                </div>
                <div class="row2Right1mBr">
                    <p>У вас есть новый хороший продукт</p>
                </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Мы красиво «упакуем» </p>
                <p>и подберем название!</p>
            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Мы красиво «упакуем»</p>
                <p> и подберем название!</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
                <ul>
                    <li>Проводим аудит бренда и вырабатываем предложения (гипотезы) по его продвижению</li>
                    <li>Разрабатываем стратегию позиционирования и платформу бренда</li>
                    <li>Создаем стратегии коммуникации бренда. </li>
                    <li>Подбираем оптимальные каналы продвижения</li>
                </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
                <ul>
                    <li>Проводим аудит бренда и вырабатываем предложения (гипотезы) по его продвижению</li>
                    <li>Разрабатываем стратегию позиционирования и платформу бренда</li>
                    <li>Создаем стратегии коммуникации бренда. </li>
                    <li>Подбираем оптимальные каналы продвижения</li>
                </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Нужна идея?</a>
        </div>

        <!-- ** КЛИЕНТИНГ ** -->



        <div class="HR">
            <hr>
        </div>


        <div class="row2">
            <div class="row2Left">
                <section id="klienting" ><h1>Клиентинг</h1></section>
                <hr>
            </div>
            <div class="row2Right">
                <img src="/public/images/question.png" alt="img" width="40" height="60">        
            </div>
            <div class="row2Right1">
                <p>Ищете новые точки взаимодействия </p>
                <p>со своим клиентом</p>
            </div>
        </div>

        <div class="row2m">
            <div class="row2Leftm">
                <section id="klienting" ><h1>Клиентинг</h1></section>
                <hr>
            </div>
            <div class="row2f">
                <div class="row2Rightm">
                    <img src="/public/images/question.png" alt="img" width="40" height="60">        
                </div>
                <div class="row2Right1mBr">
                    <p>Ищете новые точки взаимодействия со своим клиентом</p>
                </div>
            </div>
        </div>


        <div class="row3">
            <div class="row3left">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1">
                <p>Мы точно настроим и донесем 
                    ваше предложение каждому клиенту !</p> 

            </div>
            <div class="row3left2">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>


        <div class="row3m">
            <div class="row3leftm">
                <img src="/public/images/kavychka_verh.png" alt="img" width="20" height="20">
            </div>
            <div class="row3left1m">
                <p>Мы точно настроим и донесем  </p>         
                <p>ваше предложение каждому клиенту !</p>
            </div>
            <div class="row3left2m">
                <img src="/public/images/kavychka_niz.png" alt="img" width="20" height="20">
            </div>
        </div>

        <div class="row4">
            <div class="row4left">
                <ul>
                    <li>Анализируем портрет клиента, определяем его потребности и запросы</li>
                    <li>Предлагаем наиболее эффективные «точки касания» с клиентом </li>
                    <li>Разрабатываем стратегию коммуникации  </li>
                    <li>Разрабатываем барьеры удержания клиентов</li>
                </ul>
            </div>
        </div>

        <div class="row4m">
            <div class="row4leftm">
                <ul>
                    <li>Анализируем портрет клиента, определяем его потребности и запросы</li>
                    <li>Предлагаем наиболее эффективные «точки касания» с клиентом </li>
                    <li>Разрабатываем стратегию коммуникации  </li>
                    <li>Разрабатываем барьеры удержания клиентов</li>
                </ul>
            </div>
        </div>

        <div class="row5">
            <a href="#contacts">Нужны клиенты?</a>
        </div>

        <!-- ** Проекты ** -->

        <div class="row2">
            <div class="row2Left">
                <section id="projects" ><h1>Проекты</h1></section>
                <hr>
            </div>
        </div>

        <div class="row2m">
            <div class="row2Leftm">
                <section id="projects" ><h1>Проекты</h1></section>
                <hr>
            </div>
        </div>


        <!--Тус блок с постами -->

        <div class="container2">       

            <?php if (empty($vars['list'])): ?>
                <p>Список Новостей пуст</p>
            <?php else: ?>
                <?php foreach ($vars['list'] as $val): ?>

                    <div class="wrapperpost">
                        <a href="/post/<?php echo $val['id']; ?>">
                            <div class="postcontainer" style="background-image: url('/public/materials/300x167/<?php echo $val['id']; ?>.jpg')">
                            </div>
                            <div class="subtitlecontainer">
                                <h2 class="post-subtitle"><?php echo htmlspecialchars($val['description'], ENT_QUOTES); ?></h2>
                            </div>
                            <h5 class="post-title"><?php echo htmlspecialchars($val['name'], ENT_QUOTES); ?></h5> 
                        </a>
                    </div>

                <?php endforeach; ?>

        <div class="row5">
            <a href="<?php echo '/posts';?>">Все проекты</a>
        </div>
            <?php endif; ?>     
        </div>


        <!--Тус блок с постами -->


        <div class="block-C ">

            <div class="Contacts-blockm">
                <div class="contacts-menu-m">
                    <h1>Контакт</h1>
                </div>

                <section id="contacts" ></section>

                <div class="contacts-menu">
                    <h1>Контакт</h1>
                    <table>
                        <tr>
                            <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 723 40 25 </h2></td>     
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 535 594 761  </h2></td>  
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 680 40 40</h2></td>  
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 515 952 543</h2></td>  
                        </tr>                
                        <tr>
                            <td class="mail"><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                            <td><h2>sales@gid.company</h2></td>  
                        </tr>
                    </table>
                </div>

                <div class="contacts-menu1-m">
                    <div class="contactpost-m">
                        <form action="/contact" method="post">
                            <div class="contactpost1">                    
                                <p><input type="text" class="form-control" name="name" placeholder="Имя"></p>                  
                            </div>
                            <div class="contactpost2">             
                                <p><input type="text" class="form-control" name="email" placeholder="E-mail"></p>
                            </div>
                            <div class="contactpost3">                    
                                <p><textarea rows="5" class="form-control" name="text" placeholder="Сообщение"></textarea></p>
                            </div>

                            <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                            <br>
                            <div id="success">
                                <button type="submit" class="btn btn-secondary" id="sendMessageButton">Выслать</button>     
                            </div>         
                        </form>
                    </div>
                </div>

                <div class="contacts-menu-m1">
                    <table>
                        <tr>
                            <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 723 40 25 </h2></td><td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 535 594 761  </h2></td>      
                        </tr>
                        <tr>
                            <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+375 29 680 40 40</h2></td><td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                            <td><h2>+48 515 952 543</h2></td>   
                        </tr>
                        <tr>

                        </tr>   

                    </table>
                    <table align="center">
                        <tr >
                            <td class="mail" ><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                            <td><h2>sales@gid.company</h2></td>  
                        </tr>
                    </table> 
                </div>

            </div>

        </div>

        <div class="facebook">                     
            <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
        </div>

    </div>