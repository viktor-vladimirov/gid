<div class="wrapper">

    <div class="block-A">

        <div class="menu">
            <a class="menu-left" href="/"></a>

            <div class="dropdown">
                <button class="mainmenubtn"></button>
                <div class="dropdown-child">
                    <div class="drop">
                        <a href="/" class="close"></a>
                    </div>
                    <div class="dropr1">
                        <a href="/" class="launge1">ru</a>
                        <a href="/pl" class="launge2">pl</a>
                        <a href="/en" class="launge3">en</a>
                    </div>
                    <div class="dropr2">
                        <a href="/projects">Проекты</a>
                        <a href="/aboutus">О нас</a>
                        <a href="/contact">Контакты</a>
                    </div>
                </div>
            </div>

            <div class="menu-right">
                <a href="/">pl</a>
                <a href="/">en</a>
                <a href="/">ru</a>
            </div>
        </div>
        <div class="menu-right1">
            <a href="/posts">Проекты</a>
            <a href="/aboutus">О нас</a>
            <a href="#contacts">Контакты</a>
        </div>
        <div class="menu1">
            <p>О нас</p>
            <hr>
            <h1>GID - Команда профессионалов, мы готовы предложить
                вам конструктивные решения любых маркетинговых задач.</h1>
        </div>
        <div class="menu2">
            <a href="/#strategy">Стратегия</a>
            <a href="/#analytics">Аналитика</a>
            <a href="/#branding">Брендинг</a>
            <a href="/#klienting">Клиентинг</a>
        </div>

    </div>

    <div class="block-D">

        <div class="row-us1">
            <h1>О нас</h1>
            <hr>
        </div>

        <div class="row-us2">
            <text>
            Если у вас есть необходимость исследовать рынок, проанализировать деятельность конкурентов, выявить или создать конкурентные преимущества - наша команда GID Consulting поможет реализовать комплексные маркетинговые проекты. Если планируете выход на рынки СНГ, мы готовы предложить стратегии продвижения, сформировать или адаптировать бренд, разработать упаковку.
            Наша команда это практикующие специалисты с опытом работы в маркетинге и продажах на крупных коммерческих и производственных предприятиях Беларуси. Сегодня мы хотим предложить вам наши знания, навыки и опыт в решении ваших комплексных маркетинговых задач .
            </text>
        </div>

        <div class="row-us1-2">
            <h1>GID Команда</h1>
            <hr>
        </div>

        <div class="row-us3b1">
            <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Любовь Ковалева</h2>
                <div class="Pb"><p>Стратегия, Брендинг</p></div>
                <hr>
                <p>Опыт работы: 15 лет</p>
                <p>ООО "Санта-Бремор"</p>
                <p>ООО "Санта Рест"</p>
                <p> ООО "СТиМ"</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 296 42 90 80</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>lubov.kovaleva@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

            <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Екатерина Зарькова</h2>
                <div class="Pb"><p>Брендинг, Проект Менеджмент</p></div>
                <hr>
                <p>Опыт работы: 8 лет</p>
                <p>ООО "СТиМ"</p>
                <p>ООО "Трансконсалт"</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 297 980 626</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

            <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Дмитрий Ковалев</h2>
                <div class="Pb"><p>Проект менеджмент , Продажи</p></div>
                <hr>
                <p>Опыт работы: 15 лет в продажах</p>
                <p>строительных материалов и транспортных услуг</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+48 573 913 300</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>dmitry.kovalev@gid.company</h3></td>  
                    </tr>
                </table>
            </div>
        </div>

        <div class="row-us3b2">
            <div class="persona1">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Ирина Турянская</h2>
                <div class="Pb"><p>Клиентинг, Аналитика. Продажи </p></div>
                <hr>
                <p>Опыт работы: </p>
                <p>Pуководитель отдела продаж в рекламно-информационном издательстве "Белфакта Медиа"</p>
                <p>Mаркетолог в ГК "СТиМ", руководитель образовательного проекта "Первый шаг"(Брест)</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 29 723 40 25</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>irina.turanskaya@gid.company</h3></td>  
                    </tr>
                </table>
            </div>    
        </div>   

        <!-- m -->

        <div class="row-us3b1m">
            <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Любовь Ковалева</h2>
                <div class="Pb"><p>Стратегия, Брендинг</p></div>
                <hr>
                <p>Опыт работы: 15 лет</p>
                <p>ООО "Санта-Бремор"</p>
                <p>ООО "Санта Рест"</p>
                <p> ООО "СТиМ"</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 296 42 90 80</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>lubov.kovaleva@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

            <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Екатерина Зарькова</h2>
                <div class="Pb"><p>Брендинг, Проект Менеджмент</p></div>
                <hr>
                <p>Опыт работы: 8 лет</p>
                <p>ООО "СТиМ"</p>
                <p>ООО "Трансконсалт"</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 297 980 626</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

        </div>   



        <div class="row-us3b1m">            
            <div class="persona">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Ирина Турянская</h2>
                <div class="Pb"><p>Клиентинг, Аналитика. Продажи </p></div>
                <hr>
                <p>Опыт работы: </p>
                <p>Pуководитель отдела продаж в рекламно-информационном издательстве "Белфакта Медиа"</p>
                <p>Mаркетолог в ГК "СТиМ", руководитель образовательного проекта "Первый шаг"(Брест)</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 29 723 40 25</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>irina.turanskaya@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

            <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Дмитрий Ковалев</h2>
                <div class="Pb"><p>Проект менеджмент , Продажи</p></div>
                <hr>
                <p>Опыт работы: 15 лет в продажах</p>
                <p>строительных материалов и транспортных услуг</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+48 573 913 300</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>dmitry.kovalev@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

        </div>

        <!-- m1 -->


        <div class="row-us3b1m1">
            <div class="persona">
                <img  src="/public/images/aboutus/kovaleva.jpg" alt="img" width="200" height="300">
                <h2>Любовь Ковалева</h2>
                <div class="Pb"><p>Стратегия, Брендинг</p></div>
                <hr>
                <p>Опыт работы: 15 лет</p>
                <p>ООО "Санта-Бремор"</p>
                <p>ООО "Санта Рест"</p>
                <p> ООО "СТиМ"</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 296 42 90 80</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>lubov.kovaleva@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

        </div>

        <div class="row-us3b1m1">

            <div class="persona">
                <img  src="/public/images/aboutus/zarkova.jpg" alt="img" width="200" height="300">
                <h2>Екатерина Зарькова</h2>
                <div class="Pb"><p>Брендинг, Проект Менеджмент</p></div>
                <hr>
                <p>Опыт работы: 8 лет</p>
                <p>ООО "СТиМ"</p>
                <p>ООО "Трансконсалт"</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 297 980 626</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>ekaterina.zarkova@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

        </div>         

        <div class="row-us3b1m1">

            <div class="persona">
                <img  src="/public/images/aboutus/kovalev.jpg" alt="img" width="200" height="300">
                <h2>Дмитрий Ковалев</h2>
                <div class="Pb"><p>Проект менеджмент , Продажи</p></div>
                <hr>
                <p>Опыт работы: 15 лет в продажах</p>
                <p>строительных материалов и транспортных услуг</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+48 573 913 300</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>dmitry.kovalev@gid.company</h3></td>  
                    </tr>
                </table>
            </div>

        </div>

        <div class="row-us3b1m1">   
            <div class="persona">
                <img  src="/public/images/aboutus/turianskaya.jpg" alt="img" width="200" height="300">
                <h2>Ирина Турянская</h2>
                <div class="Pb"><p>Клиентинг, Аналитика. Продажи </p></div>
                <hr>
                <p>Опыт работы: </p>
                <p>Pуководитель отдела продаж в рекламно-информационном издательстве "Белфакта Медиа"</p>
                <p>Mаркетолог в ГК "СТиМ", руководитель образовательного проекта "Первый шаг"(Брест)</p>
                <hr>
                <table>
                    <tr>
                        <td class="viberP"><img src="/public/images/viber-ab.png" alt="img" width="20" height="20"></td>
                        <td><h3>+375 29 723 40 25</h3></td>     
                    </tr>
                    <tr>
                        <td class="mailP"><img src="/public/images/mail-ab.png" alt="img" width="25" height="20"></td>
                        <td><h3>irina.turanskaya@gid.company</h3></td>  
                    </tr>
                </table>
            </div>
        </div>


    </div>    

    <div class="block-C ">

        <div class="Contacts-blockm">
            <div class="contacts-menu-m">
                <h1>Контакт</h1>
            </div>

            <section id="contacts" ></section>

            <div class="contacts-menu">
                <h1>Контакт</h1>
                <table>
                    <tr>
                        <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                        <td><h2>+375 29 723 40 25 </h2></td>     
                    </tr>
                    <tr>
                        <td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                        <td><h2>+48 535 594 761  </h2></td>  
                    </tr>
                    <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                    <td><h2>+375 29 680 40 40</h2></td>  
                    </tr>
                    <tr>
                        <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                        <td><h2>+48 515 952 543</h2></td>  
                    </tr>                
                    <tr>
                    <tr>
                        <td class="mail"><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                        <td><h2>sales@gid.company</h2></td>  
                    </tr>
                </table>
            </div>

            <div class="contacts-menu1-m">
                <div class="contactpost-m">
                    <form action="/contact" method="post">
                        <div class="contactpost1">                    
                            <p><input type="text" class="form-control" name="name" placeholder="Имя"></p>                  
                        </div>
                        <div class="contactpost2">             
                            <p><input type="text" class="form-control" name="email" placeholder="E-mail"></p>
                        </div>
                        <div class="contactpost3">                    
                            <p><textarea rows="5" class="form-control" name="text" placeholder="Сообщение"></textarea></p>
                        </div>

                        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">

                        <br>
                        <div id="success">
                            <button type="submit" class="btn btn-secondary" id="sendMessageButton">Выслать</button>     
                        </div>         
                    </form>
                </div>
            </div>

            <div class="contacts-menu-m1">
                <table>
                    <tr>
                        <td class="viber"><img src="/public/images/viber.png" alt="img" width="20" height="20"></td>
                        <td><h2>+375 29 723 40 25 </h2></td><td class="whatsupp"><img src="/public/images/wht.png" alt="img" width="20" height="20"></td>
                        <td><h2>+48 535 594 761  </h2></td>      
                    </tr>
                    <tr>
                        <td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                        <td><h2>+375 29 680 40 40</h2></td><td class="whatsupp"><img src="/public/images/phone.png" alt="img" width="20" height="20"></td>
                        <td><h2>+48 515 952 543</h2></td>   
                    </tr>
                    <tr>

                    </tr>   

                </table>
                <table align="center">
                    <tr >
                        <td class="mail" ><img src="/public/images/mail.png" alt="img" width="25" height="20"></td>
                        <td><h2>sales@gid.company</h2></td>  
                    </tr>
                </table> 
            </div>

        </div>

    </div>  

    <div class="facebook">                     
        <a class="fb" href="https://www.facebook.com/GIDproconsult/"></a>
    </div>

</div>