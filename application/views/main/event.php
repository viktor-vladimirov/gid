<div class="wrapper">
	    <header> 
    <a href="/">Projects</a>
    <a href="/event">Event</a>   
    <a href="/">Analityka</a> 
    <a href="/">Branding</a> 
    <a href="#contact">Contacts</a> 
    </header>
    
	<div class="headimg"></div>
</div>
<div class="evwrapper0">
<div class="evwrapper">
	<div class="evtextcontainer1">
		<h1>Приглашаем на workshop</h1>
		<h2>«Каково это - зарабатывать с UBER ?»</h2>
	</div>

	<div class="evtextcontainer2">
		<div class="evtittle1">
			<p>Будем делиться:</p>
			<p>- личным опытом</p>
			<p>- ошибками и открытиями</p>
			<p>- очевидными вещами</p>		
		</div>
		<div class="evtittle1">

			<p>Будем говорить:</p>
			<p>- о схемах работы, финансовых планах и реальности</p>
			<p>- о типах клиентов, научимся работать с возражениями </p>
			<p>- о пользовании приложением</p>
			<p>- о нюансах, о которых обычно не говорят</p>
			<p>- о мифах и реальности</p>				
		</div>
		<div class="evtittle1">
			<p>Послушаем экспертов:</p>
			<p>- нюансы партнёрства с Убер</p>
			<p>- особенности бухгалтерского учета</p>
					
		</div>
			<div class="evtittle1">
			<p>Место: ul. Świętego Filipa, 17, Kraków, Poland</p>
			<p>Время: 14:30-17:30</p>
			<p>Дата: 21 июня 2019 г.</p>
			<p>Стоимость билетa: 30 злотых. Кофе-пауза входит в стоимость билета :)</p>		
		</div>
	</div>	
</div>

<div class="wrapper">
	<div class="evtextcontainer3">
    <div class="evrow">
        <a href="https://837412-1.evenea.pl/index/index">Купить билет</a>
        </div>      
    </div>

	<div class="container7">
       <section id="contact"><h1>Kontakt</h1></section>
    </div>
    <div class="container8">
        <div class="row3">
            <table>
                <tr>
                <td class="viber"><img src="/public/images/viber.jpg" alt="img" width="20" height="20"></td>
                <td><h2>+375 29 723 40 25 </h2></td>     
                </tr>
                <tr>
                <td class="whatsupp"><img src="/public/images/wht.jpg" alt="img" width="20" height="20"></td>
                <td><h2>+48 535 594 761  </h2></td>  
                </tr>
                <tr>
                <td class="mail"><img src="/public/images/mail.jpg" alt="img" width="20" height="20"></td>
                <td><h2>info@gid.company</h2></td>  
                </tr>
            </table>
        </div>
        <div  class="row2">
            <p><a href="http://gidaccountbrest.beep.pl/event">Заказать звонок</a></p>
        </div>

    </div>
    <div class="footimg"></div>
</div>	